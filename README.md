Kod realizuje zadanie: 

Zaimplementuj model umożliwiający klientowi zakup produktu. 

Uwzględnij dwie reguły biznesowe: 

- produkty są unikalne i mogą zostać kupione tylko jeden raz 

- produkty mogą zostać kupione tylko przez pełnoletniego klienta (musi mieć 18 lub więcej lat) 

Do powyższej implementacji napisz testy jednostkowe.

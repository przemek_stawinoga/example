<?php

namespace Contracts;

use DateTimeImmutable;

interface UsersFacadeInterface
{
    public function add(string $identifier, DateTimeImmutable $dateOfBirth): void;

    public function exist(string $identifier): bool;

    public function isAdult(string $identifier): bool;
}

<?php

namespace Contracts;

interface OrdersFacadeInterface
{
    public function placeOrder(string $userIdentifier, string $productIdentifier): void;

    public function hasUserOrderWithProduct(string $userIdentifier, string $productIdentifier): bool;
}

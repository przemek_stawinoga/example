<?php

namespace Contracts;

interface ProductsFacadeInterface
{
    public function add(string $identifier, string $name): void;

    public function exist(string $identifier): bool;
}

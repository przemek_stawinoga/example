<?php

namespace Tests\Users;

use DateTimeImmutable;
use Users\Exceptions\InvalidUserDateOfBirth;
use Users\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    use UsersFixturesTrait;

    public function testUserHasCorrectStateAfterCreation(): void
    {
        $identifier  = $this->identifier();
        $dateOfBirth = $this->dateOfBirth();

        $user = new User($identifier, $dateOfBirth);

        $this->assertSame($user->getIdentifier(), $identifier);
        $this->assertSame($user->getBirthDate(), $dateOfBirth);

    }

    public function testDateOfBirthCannotBeFuture(): void
    {
        $futureDate = (new DateTimeImmutable())->modify("+1 year");

        $this->expectException(InvalidUserDateOfBirth::class);
        $this->expectExceptionMessage('Date of birth cannot be future');

        new User("Test User", $futureDate);
    }

    public function testDeterminesCorrectlyWhenUserIsAnAdult(): void
    {
        $user = new User("Test User", $this->prepareAdultDate());

        $this->assertTrue($user->isAdult());
    }

    public function testDeterminesCorrectlyWhenUserIsNotAnAdult(): void
    {
        $birthDate = $this->prepareAdultDate()->modify("+1 years");

        $user = new User("Test User", $birthDate);

        $this->assertFalse($user->isAdult());
    }

    private function prepareAdultDate(): DateTimeImmutable
    {
        return (new DateTimeImmutable())->modify("- " . User::AGE_OF_ADULTHOOD_IN_POLAND . " years");
    }
}

<?php

namespace Tests\Users;

use DateTimeImmutable;
use Users\User;

trait UsersFixturesTrait
{
    private function identifier(): string
    {
        return uniqid();
    }

    private function dateOfBirth(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

    private function user(): User
    {
        return new User($this->identifier(), $this->dateOfBirth());
    }

    private function adultUser(): User
    {
        return new User(
            $this->identifier(),
            (new DateTimeImmutable())->modify("- " . User::AGE_OF_ADULTHOOD_IN_POLAND . " years")
        );
    }

    private function notAdultUser(): User
    {
        $adultUser = (new DateTimeImmutable())->modify("- " . User::AGE_OF_ADULTHOOD_IN_POLAND . " years");

        return new User(
            $this->identifier(),
            $adultUser->modify('+1 year')
        );
    }
}

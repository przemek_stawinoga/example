<?php

namespace Tests\Users;

use PHPUnit\Framework\TestCase;
use Users\Exceptions\UserAlreadyExistException;
use Users\Exceptions\UserNotFoundException;
use Users\UserRepositoryInterface;
use Users\UsersFacade;

class UsersFacadeTest extends TestCase
{
    use UsersFixturesTrait;

    public function testNewUserCanBeAdded(): void
    {
        $identifier = $this->identifier();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->once())
                       ->method('get')
                       ->with($identifier)
                       ->willReturn(null);

        $facade = new UsersFacade($userRepository);

        $facade->add($identifier, $this->dateOfBirth());
    }

    public function testUserCannotBeAddedWithThisSameIdentifier(): void
    {
        $user = $this->user();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->once())
                       ->method('get')
                       ->with($user->getIdentifier())
                       ->willReturn($user);

        $this->expectException(UserAlreadyExistException::class);

        $facade = new UsersFacade($userRepository);

        $facade->add($user->getIdentifier(), $this->dateOfBirth());
    }

    public function testCanCorrectlyCheckWhenUserNotExist(): void
    {
        $identifier = $this->identifier();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->once())
                       ->method('get')
                       ->with($identifier)
                       ->willReturn(null);

        $facade = new UsersFacade($userRepository);

        $this->assertFalse($facade->exist($identifier));
    }

    public function testCanCorrectlyCheckWhenUserExist(): void
    {
        $user = $this->user();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->once())
                       ->method('get')
                       ->with($user->getIdentifier())
                       ->willReturn($user);

        $facade = new UsersFacade($userRepository);

        $this->assertTrue($facade->exist($user->getIdentifier()));
    }

    public function testCanCorrectlyCheckWhenUserIsAdult(): void
    {
        $user = $this->adultUser();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->exactly(2))
                       ->method('get')
                       ->with($user->getIdentifier())
                       ->willReturn($user);

        $facade = new UsersFacade($userRepository);

        $this->assertTrue($facade->isAdult($user->getIdentifier()));
    }

    public function testCanCorrectlyCheckWhenUserIsNotAdult(): void
    {
        $user = $this->notAdultUser();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->exactly(2))
                       ->method('get')
                       ->with($user->getIdentifier())
                       ->willReturn($user);

        $facade = new UsersFacade($userRepository);

        $this->assertFalse($facade->isAdult($user->getIdentifier()));
    }

    public function testCannotCheckAdultityWhenUserNotfound(): void
    {
        $user = $this->user();

        $userRepository = $this->createMock(UserRepositoryInterface::class);
        $userRepository->expects($this->once())
                       ->method('get')
                       ->with($user->getIdentifier())
                       ->willReturn(null);

        $this->expectException(UserNotFoundException::class);

        $facade = new UsersFacade($userRepository);

        $facade->isAdult($user->getIdentifier());
    }
}

<?php

namespace Users;

use DateTimeImmutable;
use Users\Exceptions\InvalidUserDateOfBirth;

class User
{
    const AGE_OF_ADULTHOOD_IN_POLAND = 18;

    private DateTimeImmutable  $birthDate;
    private string             $identifier;

    public function __construct(string $identifier, DateTimeImmutable $birthDate)
    {
        $this->assertBirthDate($birthDate);
        $this->identifier = $identifier;
        $this->birthDate  = $birthDate;
    }

    public function getBirthDate(): DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function isAdult()
    {
        $diff = (new DateTimeImmutable())->diff($this->birthDate);

        return $diff->y >= self::AGE_OF_ADULTHOOD_IN_POLAND;
    }

    private function assertBirthDate(DateTimeImmutable $dateTimeImmutable): void
    {
        if ($dateTimeImmutable > new DateTimeImmutable()) {
            throw new InvalidUserDateOfBirth("Date of birth cannot be future");
        }
    }
}

<?php

namespace Users;

interface UserRepositoryInterface
{
    public function get(string $identifier): ?User;

    public function add(User $user): void;
}

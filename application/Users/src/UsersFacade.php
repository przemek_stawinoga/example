<?php

namespace Users;

use Contracts\UsersFacadeInterface;
use DateTimeImmutable;
use Users\Exceptions\UserAlreadyExistException;
use Users\Exceptions\UserNotFoundException;

class UsersFacade implements UsersFacadeInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function add(string $identifier, DateTimeImmutable $dateOfBirth): void
    {
        if ($this->exist($identifier)) {
            throw new UserAlreadyExistException();
        }

        $this->userRepository->add(new User($identifier, $dateOfBirth));
    }

    public function exist(string $identifier): bool
    {
        return (bool)$this->userRepository->get($identifier);
    }

    public function isAdult(string $identifier): bool
    {
        if (!$this->exist($identifier)) {
            throw new UserNotFoundException();
        }

        return $this->userRepository->get($identifier)->isAdult();
    }
}

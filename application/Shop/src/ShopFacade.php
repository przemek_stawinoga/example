<?php

namespace Shop;

use Contracts\OrdersFacadeInterface;
use Contracts\UsersFacadeInterface;
use Shop\Exceptions\OnlyAdultUserCanBuyProductException;
use Shop\Exceptions\UserAlreadyHasOrderWithThatProduct;

class ShopFacade
{
    private OrdersFacadeInterface $ordersFacade;
    private UsersFacadeInterface $usersFacade;

    public function __construct(
        UsersFacadeInterface $usersFacade,
        OrdersFacadeInterface $ordersFacade
    ) {
        $this->usersFacade  = $usersFacade;
        $this->ordersFacade = $ordersFacade;
    }

    public function placeOrder(string $userIdentifier, string $productIdentifier)
    {
        if (!$this->usersFacade->isAdult($userIdentifier)) {
            throw new OnlyAdultUserCanBuyProductException();
        }

        if ($this->ordersFacade->hasUserOrderWithProduct($userIdentifier, $productIdentifier)) {
            throw new UserAlreadyHasOrderWithThatProduct();
        }

        $this->ordersFacade->placeOrder($userIdentifier, $productIdentifier);
    }
}

<?php

namespace Tests\Shop;

use Contracts\OrdersFacadeInterface;
use Contracts\UsersFacadeInterface;
use Shop\Exceptions\OnlyAdultUserCanBuyProductException;
use Shop\Exceptions\UserAlreadyHasOrderWithThatProduct;
use Shop\ShopFacade;
use PHPUnit\Framework\TestCase;

class ShopFacadeTest extends TestCase
{
    public function testOrderCannotBePlacedWhenUserAlreadyHasOrderWithThatProduct()
    {
        $userIdentifier    = $this->identifier();
        $productIdentifier = $this->identifier();

        $userFacade  = $this->createMock(UsersFacadeInterface::class);
        $orderFacade = $this->createMock(OrdersFacadeInterface::class);

        $userFacade->expects($this->once())
                   ->method('isAdult')
                   ->with($userIdentifier)
                   ->willReturn(true);

        $orderFacade->expects($this->once())
                    ->method('hasUserOrderWithProduct')
                    ->with($userIdentifier, $productIdentifier)
                    ->willReturn(true);

        $this->expectException(UserAlreadyHasOrderWithThatProduct::class);

        $facade = new ShopFacade($userFacade, $orderFacade);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    public function testOrderCannotBePlacedWhenUserIsNotAdult()
    {
        $userIdentifier    = $this->identifier();
        $productIdentifier = $this->identifier();

        $userFacade  = $this->createMock(UsersFacadeInterface::class);
        $orderFacade = $this->createMock(OrdersFacadeInterface::class);

        $userFacade->expects($this->once())
                   ->method('isAdult')
                   ->with($userIdentifier)
                   ->willReturn(false);

        $this->expectException(OnlyAdultUserCanBuyProductException::class);
        $facade = new ShopFacade($userFacade, $orderFacade);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    public function testUserCanPlaceOrder()
    {
        $userIdentifier    = $this->identifier();
        $productIdentifier = $this->identifier();

        $userFacade  = $this->createMock(UsersFacadeInterface::class);
        $orderFacade = $this->createMock(OrdersFacadeInterface::class);

        $userFacade->expects($this->once())
                   ->method('isAdult')
                   ->with($userIdentifier)
                   ->willReturn(true);

        $orderFacade->expects($this->once())
                    ->method('hasUserOrderWithProduct')
                    ->with($userIdentifier, $productIdentifier)
                    ->willReturn(false);

        $facade = new ShopFacade($userFacade, $orderFacade);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    private function identifier(): string
    {
        return uniqid();
    }
}

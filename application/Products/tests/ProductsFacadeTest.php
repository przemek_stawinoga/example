<?php

namespace Tests\Products;

use Products\Exceptions\ProductAlreadyExistException;
use Products\Product;
use Products\ProductRepositoryInterface;
use Products\ProductsFacade;
use PHPUnit\Framework\TestCase;

class ProductsFacadeTest extends TestCase
{
    public function testProductCannotBeAddedWhenAlreadyExist()
    {
        $productIdentifier = $this->identifier();
        $productName       = $this->name();

        $product = new Product($productIdentifier, $productName);

        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->expects($this->once())
                          ->method('get')
                          ->with($productIdentifier)
                          ->willReturn($product);

        $this->expectException(ProductAlreadyExistException::class);

        $facade = new ProductsFacade($productRepository);

        $facade->add($productIdentifier, $productName);
    }

    public function testProductCannotBeAdded()
    {
        $productIdentifier = $this->identifier();
        $productName       = $this->name();

        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->expects($this->once())
                          ->method('get')
                          ->with($productIdentifier)
                          ->willReturn(null);

        $facade = new ProductsFacade($productRepository);

        $facade->add($productIdentifier, $productName);
    }

    public function testPositiveCheckThatProductExist()
    {
        $productIdentifier = $this->identifier();
        $productName       = $this->name();

        $product = new Product($productIdentifier, $productName);

        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->expects($this->once())
                          ->method('get')
                          ->with($productIdentifier)
                          ->willReturn($product);

        $facade = new ProductsFacade($productRepository);

        $this->assertTrue($facade->exist($productIdentifier));
    }

    public function testNegativeCheckThatProductExist()
    {
        $productIdentifier = $this->identifier();

        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->expects($this->once())
                          ->method('get')
                          ->with($productIdentifier)
                          ->willReturn(null);

        $facade = new ProductsFacade($productRepository);

        $this->assertFalse($facade->exist($productIdentifier));
    }

    private function identifier(): string
    {
        return uniqid();
    }

    private function name(): string
    {
        return uniqid();
    }
}

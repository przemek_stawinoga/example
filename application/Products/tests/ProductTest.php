<?php

namespace Tests\Products;

use PHPUnit\Framework\TestCase;
use Products\Product;

class ProductTest extends TestCase
{
    public function testProductHasCorrectStateAfterCreation(): void
    {
        $identifier = $this->identifier();
        $name       = $this->name();

        $user = new Product($identifier, $name);

        $this->assertSame($user->getIdentifier(), $identifier);
        $this->assertSame($user->getName(), $name);
    }

    private function identifier(): string
    {
        return uniqid();
    }

    private function name(): string
    {
        return uniqid();
    }
}

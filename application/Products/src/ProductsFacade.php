<?php

namespace Products;

use Contracts\ProductsFacadeInterface;
use Products\Exceptions\ProductAlreadyExistException;

class ProductsFacade implements ProductsFacadeInterface
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function add(string $identifier, string $name): void
    {
        if ($this->exist($identifier)) {
            throw new ProductAlreadyExistException();
        }

        $this->productRepository->add(new Product($identifier, $name));
    }

    public function exist(string $identifier): bool
    {
        return (bool)$this->productRepository->get($identifier);
    }
}

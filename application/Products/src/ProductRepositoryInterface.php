<?php

namespace Products;

interface ProductRepositoryInterface
{
    public function get(string $identifier): ?Product;

    public function add(Product $user): void;
}

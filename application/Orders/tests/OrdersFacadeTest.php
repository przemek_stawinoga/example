<?php

namespace Tests\Orders;

use Contracts\ProductsFacadeInterface;
use Contracts\UsersFacadeInterface;
use Orders\Exceptions\ProductNotFoundException;
use Orders\Exceptions\UserNotFoundException;
use Orders\Order;
use Orders\OrderQueryInterface;
use Orders\OrderRepositoryInterface;
use Orders\OrdersFacade;
use PHPUnit\Framework\TestCase;

class OrdersFacadeTest extends TestCase
{
    public function testCannotPlaceOrderWhenProductNotExist(): void
    {
        $userIdentifier    = $this->identifier();
        $productIdentifier = $this->identifier();

        $userFacade      = $this->createMock(UsersFacadeInterface::class);
        $productFacade   = $this->createMock(ProductsFacadeInterface::class);
        $orderQuery      = $this->createMock(OrderQueryInterface::class);
        $orderRepository = $this->createMock(OrderRepositoryInterface::class);

        $productFacade->expects($this->once())
                      ->method('exist')
                      ->with($productIdentifier)
                      ->willReturn(false);

        $this->expectException(ProductNotFoundException::class);

        $facade = new OrdersFacade($userFacade, $productFacade, $orderQuery, $orderRepository);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    public function testCannotPlaceOrderWhenUserNotExist(): void
    {
        $productIdentifier = $this->identifier();
        $userIdentifier    = $this->identifier();

        $userFacade      = $this->createMock(UsersFacadeInterface::class);
        $productFacade   = $this->createMock(ProductsFacadeInterface::class);
        $orderQuery      = $this->createMock(OrderQueryInterface::class);
        $orderRepository = $this->createMock(OrderRepositoryInterface::class);

        $productFacade->expects($this->once())
                      ->method('exist')
                      ->with($productIdentifier)
                      ->willReturn(true);

        $userFacade->expects($this->once())
                   ->method('exist')
                   ->with($userIdentifier)
                   ->willReturn(false);

        $this->expectException(UserNotFoundException::class);

        $facade = new OrdersFacade($userFacade, $productFacade, $orderQuery, $orderRepository);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    public function testOrderCanBePlacedCorrectly(): void
    {
        $productIdentifier = $this->identifier();
        $userIdentifier    = $this->identifier();

        $userFacade      = $this->createMock(UsersFacadeInterface::class);
        $productFacade   = $this->createMock(ProductsFacadeInterface::class);
        $orderQuery      = $this->createMock(OrderQueryInterface::class);
        $orderRepository = $this->createMock(OrderRepositoryInterface::class);

        $productFacade->expects($this->once())
                      ->method('exist')
                      ->with($productIdentifier)
                      ->willReturn(true);

        $userFacade->expects($this->once())
                   ->method('exist')
                   ->with($userIdentifier)
                   ->willReturn(true);

        $facade = new OrdersFacade($userFacade, $productFacade, $orderQuery, $orderRepository);

        $facade->placeOrder($userIdentifier, $productIdentifier);
    }

    public function testVerificationWhenUserAlreadyHasOrderWithProductIsCorrect(): void
    {
        $productIdentifier = $this->identifier();
        $userIdentifier    = $this->identifier();

        $userFacade      = $this->createMock(UsersFacadeInterface::class);
        $productFacade   = $this->createMock(ProductsFacadeInterface::class);
        $orderQuery      = $this->createMock(OrderQueryInterface::class);
        $orderRepository = $this->createMock(OrderRepositoryInterface::class);

        $orderQuery->expects($this->once())
                   ->method('hasUserAlreadyOrderWithProduct')
                   ->with($userIdentifier, $productIdentifier)
                   ->willReturn(true);

        $facade = new OrdersFacade($userFacade, $productFacade, $orderQuery, $orderRepository);

        $this->assertTrue($facade->hasUserOrderWithProduct($userIdentifier, $productIdentifier));
    }

    public function testVerificationWhenUserHasNotHaveOrderWithProductIsCorrect(): void
    {
        $productIdentifier = $this->identifier();
        $userIdentifier    = $this->identifier();

        $userFacade      = $this->createMock(UsersFacadeInterface::class);
        $productFacade   = $this->createMock(ProductsFacadeInterface::class);
        $orderQuery      = $this->createMock(OrderQueryInterface::class);
        $orderRepository = $this->createMock(OrderRepositoryInterface::class);

        $orderQuery->expects($this->once())
                   ->method('hasUserAlreadyOrderWithProduct')
                   ->with($userIdentifier, $productIdentifier)
                   ->willReturn(false);

        $facade = new OrdersFacade($userFacade, $productFacade, $orderQuery, $orderRepository);

        $this->assertFalse($facade->hasUserOrderWithProduct($userIdentifier, $productIdentifier));
    }

    private function identifier(): string
    {
        return uniqid();
    }
}

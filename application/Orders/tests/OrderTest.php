<?php

namespace Tests\Orders;

use Orders\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    public function testOrderHasCorrectStateAfterCreation(): void
    {
        $userIdentifier    = $this->identifier();
        $productIdentifier = $this->name();

        $order = new Order($userIdentifier, $productIdentifier);

        $this->assertSame($order->getUserIdentifier(), $userIdentifier);
        $this->assertSame($order->getProductIdentifier(), $productIdentifier);
    }

    private function identifier(): string
    {
        return uniqid();
    }

    private function name(): string
    {
        return uniqid();
    }
}

<?php

namespace Orders;

interface OrderQueryInterface
{
    public function hasUserAlreadyOrderWithProduct(string $userIdentifier, string $productIdentifier): bool;
}

<?php

namespace Orders;

class Order
{
    private string $productIdentifier;
    private string $userIdentifier;

    public function __construct(string $userIdentifier, string $productIdentifier)
    {
        $this->userIdentifier    = $userIdentifier;
        $this->productIdentifier = $productIdentifier;
    }

    public function getProductIdentifier(): string
    {
        return $this->productIdentifier;
    }

    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }
}

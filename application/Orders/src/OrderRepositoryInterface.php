<?php

namespace Orders;

interface OrderRepositoryInterface
{
    public function add(Order $order): void;
}

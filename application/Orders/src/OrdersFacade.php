<?php

namespace Orders;

use Contracts\OrdersFacadeInterface;
use Contracts\ProductsFacadeInterface;
use Contracts\UsersFacadeInterface;
use Orders\Exceptions\ProductNotFoundException;
use Orders\Exceptions\UserNotFoundException;

class OrdersFacade implements OrdersFacadeInterface
{
    private OrderQueryInterface $orderQuery;
    private OrderRepositoryInterface $orderRepository;
    private ProductsFacadeInterface $productsFacade;
    private UsersFacadeInterface $usersFacade;

    public function __construct(
        UsersFacadeInterface $usersFacade,
        ProductsFacadeInterface $productsFacade,
        OrderQueryInterface $orderQuery,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->usersFacade     = $usersFacade;
        $this->productsFacade  = $productsFacade;
        $this->orderQuery      = $orderQuery;
        $this->orderRepository = $orderRepository;
    }

    public function placeOrder(string $userIdentifier, string $productIdentifier): void
    {
        if (!$this->productsFacade->exist($productIdentifier)) {
            throw new ProductNotFoundException();
        }

        if (!$this->usersFacade->exist($userIdentifier)) {
            throw new UserNotFoundException();
        }

        $this->orderRepository->add(new Order($userIdentifier, $productIdentifier));
    }

    public function hasUserOrderWithProduct(string $userIdentifier, string $productIdentifier): bool
    {
        return $this->orderQuery->hasUserAlreadyOrderWithProduct($userIdentifier, $productIdentifier);
    }
}
